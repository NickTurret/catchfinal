package com.example.acatch

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_difficulty.*


class DifficultyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_difficulty)
        init()
    }

    private fun init() {
        easyDifficultyButton.setOnClickListener {
            val intent = Intent(this, GameActivity::class.java)
            startActivity(intent)
        }

        hardDifficultyButton.setOnClickListener {
            val alert = AlertDialog.Builder(this)
            alert.setTitle("To play hard difficulty you need to log in.")
            alert.setMessage("Are you sure you want to log in/sign up?")
            alert.setPositiveButton(
                "Yes",
                { dialogInterface: DialogInterface, i: Int ->
                    startActivity(
                        Intent(
                            this, AuthenticationActivity::class.java
                        )
                    )
                })
            alert.setNegativeButton("No", { dialogInterface: DialogInterface, i: Int -> })
            alert.show()
        }
    }
}
