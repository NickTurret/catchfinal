package com.example.acatch

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainMenuActivity : AppCompatActivity() {

    override fun onBackPressed() {
        val alert = AlertDialog.Builder(this)
        alert.setTitle("Catch")
        alert.setMessage("Are you sure you want to quit?")
        alert.setPositiveButton(
            "Yes",
            { dialogInterface: DialogInterface, i: Int -> finishAffinity() })
        alert.setNegativeButton("No", { dialogInterface: DialogInterface, i: Int -> })
        alert.show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        playButton.setOnClickListener {
            val intent = Intent(this, DifficultyActivity::class.java)
            startActivity(intent)
        }

        quitButton.setOnClickListener {
            val alert = AlertDialog.Builder(this)
            alert.setTitle("Catch")
            alert.setMessage("Are you sure you want to quit?")
            alert.setPositiveButton(
                "Yes",
                { dialogInterface: DialogInterface, i: Int -> finishAffinity() })
            alert.setNegativeButton("No", { dialogInterface: DialogInterface, i: Int -> })
            alert.show()
        }
    }
}
