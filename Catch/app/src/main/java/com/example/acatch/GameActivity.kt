package com.example.acatch

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.view.View
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_game_activty.*
import java.util.*

class GameActivity : AppCompatActivity() {
    var score: Int = 0
    var imageArray = ArrayList<ImageView>()
    var handler: Handler = Handler()
    var runnable: Runnable = Runnable { }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_activty)

        resetButton.visibility = View.INVISIBLE
        init()
        hideImages()
        resetButton()
        imageArray = arrayListOf(
            imageView1,
            imageView2,
            imageView3,
            imageView4,
            imageView5,
            imageView6,
            imageView7,
            imageView8,
            imageView9
        )


        object : CountDownTimer(15000, 1000) {
            override fun onFinish() {
                timerTextView.text = "Time out!"
                handler.removeCallbacks(runnable)
                for (each in imageArray) {
                    each.visibility = View.INVISIBLE
                }
                resetButton.visibility = View.VISIBLE
            }

            override fun onTick(millisUntilFinished: Long) {
                timerTextView.text = "Time: " + millisUntilFinished / 1000
            }

        }.start()
    }

    private fun init() {
        imageView1.setOnClickListener {
            increaseScore(imageView1)
        }
        imageView2.setOnClickListener {
            increaseScore(imageView2)
        }
        imageView3.setOnClickListener {
            increaseScore(imageView3)
        }
        imageView4.setOnClickListener {
            increaseScore(imageView4)
        }
        imageView5.setOnClickListener {
            increaseScore(imageView5)
        }
        imageView6.setOnClickListener {
            increaseScore(imageView6)
        }
        imageView7.setOnClickListener {
            increaseScore(imageView7)
        }
        imageView8.setOnClickListener {
            increaseScore(imageView8)
        }
        imageView9.setOnClickListener {
            increaseScore(imageView9)
        }

        mainMenuButton.setOnClickListener {
            val intent = Intent(this, MainMenuActivity::class.java)
            startActivity(intent)
        }
    }


    private fun hideImages() {

        runnable = object : Runnable {
            override fun run() {
                for (each in imageArray) {
                    each.visibility = View.INVISIBLE
                }


                val random = Random()
                val index = random.nextInt(9 - 0)
                imageArray[index].visibility = View.VISIBLE
                handler.postDelayed(runnable, 500)

            }
        }
        handler.post(runnable)
    }

    private fun increaseScore(view: View) {
        score++
        scoreTextView.text = "Score: " + score
    }

    private fun resetFunction() {
        hideImages()
        score = 0
        scoreTextView.text = "Score: 0"
        object : CountDownTimer(15000, 1000) {
            override fun onFinish() {
                timerTextView.text = "Time out!"
                handler.removeCallbacks(runnable)
                for (each in imageArray) {
                    each.visibility = View.INVISIBLE
                }
                resetButton.visibility = View.VISIBLE
            }

            override fun onTick(millisUntilFinished: Long) {
                timerTextView.text = "Time: " + millisUntilFinished / 1000
            }

        }.start()
    }

    private fun resetButton() {
        resetButton.setOnClickListener {
            if (timerTextView.text == "Time out!") {
                resetFunction()
                resetButton.visibility = View.INVISIBLE
            }
        }
    }
}
