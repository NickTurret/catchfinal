package com.example.acatch

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_log_in.*

private lateinit var auth: FirebaseAuth

class LogInActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)
        logIn()
    }

    private fun logIn() {

        auth = FirebaseAuth.getInstance()
        logInButton.setOnClickListener() {
            if (emailEditText.text.isEmpty() || passwordEditText.text.isEmpty()) {
                Toast.makeText(this, "Please fill in all fields!", Toast.LENGTH_SHORT).show()
            } else {
                auth.signInWithEmailAndPassword(
                    emailEditText.text.toString(),
                    passwordEditText.text.toString()
                )
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("logIn", "signInWithEmail:success")
                            val user = auth.currentUser
                            val intent = Intent(this, HardGameActivity::class.java)
                            startActivity(intent)
                            emailEditText.text.clear()
                            passwordEditText.text.clear()
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("logIn", "signInWithEmail:failure", task.exception)
                            Toast.makeText(
                                baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT
                            ).show()

                        }
                    }
                // ...
            }
        }
    }
}
